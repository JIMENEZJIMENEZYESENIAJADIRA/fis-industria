# MAPA 1.- INDUSTRIA 4.0
```plantuml
@startmindmap
*[#cyan] CAMBIOS QUE NOS ESPERAN 
	*[#Pink] > Articulos cientificos \n > Prensa(Periodico) \n > Documentales(TV) \n > Libros \n > Foros \n > Conferencias
	*[#Yellow] Involucra Tecnologias
		*[#Beige] > Robotica \n > Big Data \n > Computacion en la nube \n > Sistemas Autonomos \n > Internet \n > Inteligencia Artificial
	*[#Pink] Revoluciones Industriales 
		*[#Yellow] 1.0 >> 1784  
			*[#Beige] >> Primera revolución industrial \n >> Producción mecánica \n >> Equipos basados en energía a vapor y energía hidráulica \n >> Producción de reino unido aumento (año:1760 - 30.00 ton al año:1810 - 1.000.000 ton)
		*[#Pink] 2.0 >> 1870 
			*[#Yellow] >> Línea de ensamblaje \n >> Energía eléctrica \n >> Producción en masa \n >> Producción de carros: incremento de 800%
		*[#Beige] 3.0 >> 1969 
			*[#Pink] >> Producción automatizada usando electrónica y Tecnología de la Información \n >> Controladora lógica programable 
		*[#Yellow] 4.0 >> Actualidad 
			*[#Beige] >> Producción inteligente con decisión autónoma usando tecnologías como:
				*[#Pink] >> LoT \n >> IA \n >> Big Data \n >> Cloud \n >> Robot \n >> etc
	*[#Yellow] Cambios 
		*[#Beige] 1 >> Integración de TICs en la industria de manufacturas y de servicios
			*[#Pink] Efecto 1: Reducción de puestos de trabajo
				*[#Yellow] >> ”47% de trabajos desaparecerán en los próximos 25 años” \n >> 90 – 100%: modelos, arbitro, jueces, personal de telemercadeo \n >> 80 – 90%: taxistas, cajeros de bancos, pescadores, cocineros de comida rápida \n >> Estos serán reemplazados por robots
			*[#Beige] Efecto 2: Aparición de nuevas profesiones 
				*[#Pink] >> “65% de los estudiantes de primaria, tendrán profesiones que no existen en la actualidad” \n >> “85% de los trabajos a existir en 2030, no han sido inventados todavía”
			*[#Yellow] >> Hace 10 años no teníamos profesiones como: 
				*[#Beige] >> Youtubers \n >> conductores de Uber \n >> Desarrolladores de Apps \n >> Pilotos drones Y consultores de redes sociales con las nuevas tecnologías nos permite generar nuevas formas de hacer dinero \n >> ¡Lo mismo va a pasar en el futuro¡
		*[#Pink] 2 >> Transformación de las “empresas de manufactura” en “empresas de TICs”
			*[#Yellow] >> ANTES: Empresas de manufactura de electrodomésticos 
			*[#Beige] >> AHORA: Los electrodomésticos cuentan con sistema operativo, procesamiento de lenguaje natural, computación en la nube, comunicación inalámbrica, aplicaciones (software) 
		*[#Pink] 3 >> Nuevos paradigmas y tecnologías
			*[#Yellow] >> Negocios basados en plataformas
				*[#Beige] >> FANG : INICIALES DE:  \n >> FACEBOOK  \n >> AMAZON  \n >> NETFLIX  \n >> GOOGLE
				*[#Pink] >> BAT: INICIALES DE: 
				*[#Yellow] BAIDU \n >> ALIBABA \n >> TENCENT 
			*[#Beige] >> “EL RECURSO MAS VALIOSO YA NO ES EL PETROLEO, SINO LOS DATOS”
		*[#Pink] 4 >> Nuevas culturas digitales
@endmindmap

```
# MAPA 2.-MÉXICO Y LA INDUSTRIA 4.0
```plantuml
@startmindmap
*[#cyan] MÉXICO Y LA INDUSTRIA 4.0
	*[#Pink] "MÉXICO: HACIA LA INDUSTRIA 4.0"
		*[#Yellow] CONCEPTO NACIDO EN ALEMANIA
		*[#Beige] INTRODUCCIÓN DE LAS TECNOLOGIAS DIGITALES
		*[#Pink] CONOCIDO TAMBIEN COMO:
			*[#Yellow] LA 4ta TRASNFORMACIÓN INDUSTRIAL
		*[#Beige] STARTUP MÉXICO:
			*[#Pink] CAMPUS ESPECIALIZADO EN: \n INNOVACIÓN Y EMPRENDIMIENTO EN MÉXICO
			*[#Yellow] SE ENCUENTRAN:
				*[#Beige] >> PRINCIPALES ACTORES DEL ECOSISTEMA EMPRENDEDOR
				*[#Pink] >> FONDOS DE INVERSIÓN 
				*[#Yellow] >> GOBIERNO
				*[#Beige] >> EMPRENDEDORES Y EMPRESAS 
			*[#Pink] CON EL FIN DE CREAR:
				*[#Yellow] >> EMPRESAS INNOVADORAS
				*[#Beige] >> EMPRESAS EXISTOSAS
		*[#Pink] ADAPTACIÓN DE LAS TECNOLOGIAS RELACIONADAS \n A LA CUARTA REVOLUCIÓN INDUSTRIAL
			*[#Yellow] AHORRO DE COSTOS ASTA UN 30%
		*[#Beige] OBTENIENDO:
			*[#Pink] BENEFICIOS:
				*[#Yellow] >> MÉXICO SE ENCUENTRA EN EL PUNTO DE INFLEXIÓN EN LA INDUSTRIA 4.0
			*[#Beige] INCONVENIENTES:
				*[#Pink] >> PERDIDA DE EMPLEO
		*[#Yellow] PROFESOR DE HARVARD: \n "EN EL FUTURO EXISTIRAN DOS TIPOS DE EMPLEO"
			*[#Beige] 1.- PERSONAS QUE LE DIGAN A LAS MAQUINAS QUE HACER \n 2.- PERSONAS QUE RECIBAN ORDENES DE LAS MAQUINAS 
	*[#Pink] CONFERENCIA MAGISTRAL "MÉXICO EN LA INDUSTRIA 4.0"
		*[#Yellow] ECONOMIA CREATIVA 
		*[#Beige] SE CREAN PERFILES LABORALES A FUTURO
		*[#Pink] LA INDUSTRIA 4.0 ES YA UN HECHO Y ESTARÁ EN FUNCIÓN CON LAS PERSONAS
		*[#Yellow] SE PROVECHAN NUEVAS TECNOLOGIAS 
		*[#Beige] ¿QUÉ SE ESPERA? 
			*[#Pink] >> RECORTE DE COSTOS \n >> OPTIMIZACIÓN \n >> CREACIÓN DE NUEVAS ÁREAS DE NEGOCIO
		*[#Yellow] LA TECNOLOGÍA PROCESA Y RECOLECTA DATOS EN TIEMPO REAL  
		*[#Beige] LA EMPRESA TIENE PROCESOS:
			*[#Pink] >> AUTOMATIZADOS \n >> SEMIAUTOMATIZADOS 
		*[#Yellow] ENTRE LA INDUSTRIA 3.0 y 4.0 
			*[#Beige] SE REQUIERE DE COMPONENTES BÁSICOS COMO:
				*[#Pink] >> COMPUTACIÓN \n >> CONECTIVIDAD \n >> VISIBILIDAD \n >> TRANSPARENCIA \n >> PREDICTIBILIDAD \n >> ADAPTABILIDAD  

@endmindmap



